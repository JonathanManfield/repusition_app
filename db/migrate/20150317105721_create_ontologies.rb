class CreateOntologies < ActiveRecord::Migration
  def change
    create_table :ontologies do |t|
      t.text :name
      t.integer :value
      t.references :experience, index: true

      t.timestamps null: false
    end
    add_foreign_key :ontologies, :experiences
    add_index :ontologies, [:experience_id, :created_at]
  end
end
