class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.text :content
      t.references :user, index: true
      t.references :place, index: true

      t.timestamps null: false
    end
    add_foreign_key :experiences, :users
    add_foreign_key :experiences, :places
    add_index :experiences, [:user_id, :created_at]
    add_index :experiences, [:place_id, :created_at]
  end
end
