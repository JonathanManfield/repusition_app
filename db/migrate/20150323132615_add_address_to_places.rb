class AddAddressToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :place_address, :string
  end
end
