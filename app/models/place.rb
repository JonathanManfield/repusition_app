class Place < ActiveRecord::Base
	has_many :experiences
	default_scope -> { order(created_at: :desc) }
end
