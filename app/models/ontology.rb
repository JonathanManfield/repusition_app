class Ontology < ActiveRecord::Base
  belongs_to :experience
  default_scope -> { order(created_at: :desc) }
  # validates :experience_id, presence: true
  before_save :downcase_fields

  def downcase_fields
  	self.name.downcase!
  end
end
