class Experience < ActiveRecord::Base
  belongs_to :user
  belongs_to :place
  has_many :ontologies, dependent: :destroy
  accepts_nested_attributes_for :ontologies, :allow_destroy => true
  default_scope -> { order(created_at: :desc) }
  # validates :user_id, presence: true
end
