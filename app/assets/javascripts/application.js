// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery_nested_form
//= require bootstrap-sprockets
//= require d3
//= require bootstrap-slider
//= require radar-chart

$(function() {

	console.log("yo");

  // wrap every 2 .experience-wrapper in .row
	var divs = $(".experience-wrapper");

	for(var i = 0; i < divs.length; i+=2) {
    divs.slice(i, i+2).wrapAll("<div class='row'></div>");
  }

  // disable enter key
  $(document).keypress(function(e) {
    if(e.which == 13) {
      if(document.location.href.indexOf('place') > -1) {
        e.preventDefault();
        return false;  
      }
    }
  });

  $(".home-jumbotron").backstretch("/images/home-backstretch.jpg");
  $(".form-wrapper").backstretch("/images/home-backstretch.jpg");
  $("#ucl-widget").backstretch("/images/ucl-backstretch.jpg");

  var paddingHeight = ($(window).height()-$(".form-wrapper").height())/2;
  $(".form-wrapper").css('padding', paddingHeight + 'px 0'); 

});

