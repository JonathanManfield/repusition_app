class PlaceController < ApplicationController

	def index
		@places = Place.all
	end

	def new
		@place = Place.new(place_params)
		if @place.save
			flash[:success] = "Place saved to database!"
			@show_place_id = @place.place_id
			redirect_to :controller => 'place', :action => 'show', :place_id => @show_place_id
		else
			flash[:danger] = "Error saving place to database. Please try again."
			render 'index'
		end
	end

	def create
		# @place = Place.new(place_params)
		# if @place.save
		# 	flash[:success] = "Place saved to database!"
		# 	redirect_to root_url
		# else
		# 	flash[:danger] = "Error saving place to database. Please try again."
		# 	render 'index'
		# end
	end

	def show
		if user_signed_in?
			@experience = current_user.experiences.build
		end
		@place_id = params[:place_id]
		@place = Place.find_by_place_id(@place_id)
		if @place
			@experiences = @place.experiences.paginate(page: params[:page])
		else
		  @place = Place.new
		end
	end

	private

		def place_params
			params.require(:place).permit(:place_id, :place_name, :place_address)
		end

end
