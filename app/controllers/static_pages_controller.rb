	class StaticPagesController < ApplicationController
  def index
  	@experience1 = Experience.first
  	@experience2 = Experience.second
  end

  def docs
  end

  def enterprise
  	@experiences = Experience.all
  end
end
