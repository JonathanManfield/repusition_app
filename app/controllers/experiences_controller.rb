class ExperiencesController < ApplicationController

	def index
		@experiences = Experience.all
	end

	def new
		# @experience = Experience.new
		@experience = current_user.experiences.build
	end

	def create
		@experience = current_user.experiences.build(experience_params)
		if @experience.save
			flash[:success] = "Experience shared!"
			@show_place_id = Place.find(params[:experience][:place_id]).place_id 
			redirect_to :controller => 'place', :action => 'show', :place_id => @show_place_id

		else
			flash[:danger] = "Error sharing experience. Please try again." 
			render 'index'
		end
	end

	def destroy
		@experience = Experience.find(params[:id])
		@experience.destroy
		flash[:success] = "Experience deleted"
		redirect_to request.referrer || root_url
	end

	private

		def experience_params
			params.require(:experience).permit(:place_id, :content,
				:ontologies_attributes => [:experience_id, :name, :value, :_destroy])
		end

end
