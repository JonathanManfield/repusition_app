class OntologyController < ApplicationController
  def new
    @ontology = Ontology.new
  end

  def create
    @ontology = Ontology.new(ontology_params)
    if @ontology.save
      flash[:success] = "Ontology saved!"
      redirect_to root_url
    else
      flash[:danger] = "Error saving ontology. Please try again." 
      render 'index'
    end
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def index
    @ontologies = Ontology.all
  end

  def show
  end

  private 

    def ontology_params
      params.require(:ontology).permit(:name, :value, :experience_id)
    end
end
