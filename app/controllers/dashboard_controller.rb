class DashboardController < ApplicationController
	before_filter :authenticate_user!

	def index
		@user = current_user
		@experience1 = Experience.first
		@experience2 = Experience.second
		@experiences = @user.experiences.paginate(page: params[:page])

	end
end
