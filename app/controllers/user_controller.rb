class UserController < ApplicationController
  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def index
  end

  def show
    @user_id = params[:user_id]
    @user = User.find_by_id(@user_id)
    if @user
      @experiences = @user.experiences.paginate(page: params[:page])
    end
  end
end
